import cv2
import numpy as np
from PIL import Image, ImageEnhance

#Using jpeg image and image contrast enhancement to filter out red pixels count accurately
firstmonth = Image.open('jack1.jpg')
secondmonth =Image.open('jack2.jpg')

contrast2 = ImageEnhance.Contrast(secondmonth)
contrast1 = ImageEnhance.Contrast(firstmonth)


frame1=contrast1.enhance(10)
frame2 =contrast2.enhance(10)

frame1.show()

frame2.show()

data1=frame1.getdata()
data2=frame2.getdata()

count1 =0
for r,g,b in data1:
	if r>250 and g==0 and b==0:
		count1 +=1

count2=0
for r,g,b in data2:
	if r>250 and g==0 and b==0:
		count2 +=1

x=count2/count1

if ((count2/count1)>1.2 ):

	print "Sorry Dude ! Keep doing more tests ... Seems like you have cancer !"
else:
	print "Rock On, Dude ! You still have time !"
