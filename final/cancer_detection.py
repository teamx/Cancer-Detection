from __future__ import division
import dicom, os
import numpy as np

path = './test'

#To select the path from the directory
paths=[os.path.join(path, f) for f in os.listdir(path)]

#sorting the paths to traverse them in the list
#files within test folder in format 1-1, 1-2, 1-3
paths.sort()

#To set the limits for the loop
length = len(paths)

#Analyzing the data
for i in range(0,length,3):
    #Reference Image
    reference_data=dicom.read_file(paths[i])
    #First Month Data
    first_month_data=dicom.read_file(paths[i+1])
    #Second Month Data
    second_month_data=dicom.read_file(paths[i+2])

    #calculating pixelarrays for comparisons
    arr=reference_data.pixel_array
    ar1=first_month_data.pixel_array
    ar2=second_month_data.pixel_array

    #Data - Difference between first month and reference image (size of red spot)
    difference1=arr-ar1
    #Data - Difference between second month and reference image (size of red spot)
    difference2=arr-ar2



    #Generealizing the array into a single list of numpy array data
    one=difference1.ravel()
    two=difference2.ravel()

    #Calculation the count of red pixels in the resulting image
    pixelcount1=(np.count_nonzero(one)/3)  #red pixel count for first month
    pixelcount2=(np.count_nonzero(two)/3)  #red pixel count for second month

    #calculating the percentage of growth
    percentage_of_growth = ((pixelcount2/pixelcount1) - 1) * 100
    #Segregating cancer patients from others
    print "---------------"
    if percentage_of_growth > 20 :

        print "\n Sorry %s ! You have cancer !!!! :( " % (reference_data.PatientName)

    elif pixelcount2 < pixelcount1:

        print "\n Hey %s ! Your growth rate reduced !" % (reference_data.PatientName)

    else:

        print "\n Hi %s ! Your dont have Cancer !" % (reference_data.PatientName)
    print "---------------"
